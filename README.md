This program provides an easy way to stream camera images from a Raspberry Pi
over a network. At the moment it encodes the camera images as a MJPEG stream
and provides a simple HTTP server that allows them to be viewed in a web
browser.

Installation
------------

Run the following to install the necessary dependencies

    sudo apt-get update
    sudo apt-get install gcc build-essential cmake vlc rpi-update

Update your Raspberry Pi's firmware if needed, using rpi-update

    # Only run this if you know what you're doing, try to compile
    # using the steps below first
    sudo rpi-update e65b8c992ad0c2486e172aee1e2b1e98d623d782

Colone the code, and then compile and install using the standard cmake way

    git clone https://bitbucket.org/DawnRobotics/raspberry_pi_camera_streamer.git
    cd raspberry_pi_camera_streamer
    mkdir build
    cd build
    cmake ../
    make
    sudo make install

Running
-------

Start up the streamer by running

    raspberry_pi_camera_streamer

View the stream in a webbrowser using the following address

    http://RASPBERRY_PI_IP_ADDRESS:8080/?action=stream

where RASPBERRY_PI_IP_ADDRESS is replaced by the IP address of your Pi

    